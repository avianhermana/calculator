export function calculator(angka1, angka2, operator) {
    if (operator == '+') {
      console.log(angka1 + angka2);
    } else if (operator == '-') {
      console.log(angka1 - angka2);
    } else if (operator == '/') {
      console.log(angka1 / angka2);
    } else if (operator == '*') {
      console.log(angka1 * angka2);
    } else if (operator.toLowerCase() == '^') {
      console.log(Math.pow(angka1, angka2));
    } else if (operator.toLowerCase() == 'max') {
      console.log(Math.max(angka1, angka2));
    } else if (operator.toLowerCase() == 'min') {
      console.log(Math.min(angka1, angka2));
    } else {
      console.log('Masukkan operator yg sesuai atau angka lain');
    }
  }
  